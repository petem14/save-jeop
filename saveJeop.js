const http = require('http');
const fs = require('fs')

const url = 'http://jservice.io/api/clues/?category='

let endNumber = 12
var finalObject = []

for (let i = 1; i <= endNumber; i++) {
    //stack overflow example code from 
    // https://stackoverflow.com/questions/16148403/using-node-js-to-connect-to-a-rest-api
    var request = http.get(url + i, function (response) {
        // data is streamed in chunks from the server
        // so we have to handle the "data" event    
        var buffer = "", data

        response.on("data", function (chunk) {
            buffer += chunk;
        });

        response.on("end", function (err) {
            // finished transferring data
            // dump the raw data
            
            data = JSON.parse(buffer);
            finalObject.push(JSON.stringify(data).slice(1, -1))
            console.log('writing')
            fs.writeFileSync('categories.json', '[' + finalObject + ']')
        });
    });
}